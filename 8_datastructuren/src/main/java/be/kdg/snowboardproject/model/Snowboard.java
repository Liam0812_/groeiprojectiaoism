package be.kdg.snowboardproject.model;

import java.time.LocalDate;
import java.util.Objects;

public class Snowboard implements Comparable<Snowboard> {
    private String brand;
    private String model;
    private double price;
    private int length;
    private RidingStyle ridingstyle;
    private LocalDate productionDate;
    public static int compareCounter = 0;
    public static int equalsCounter = 0;

    protected Snowboard(String brand, String model, double price, int length, RidingStyle ridingstyle, LocalDate productionDate) {
        setBrand(brand);
        setModel(model);
        setPrice(price);
        setLength(length);
        setRidingstyle(ridingstyle);
        setProductionDate(productionDate);
    }

    protected Snowboard() {
        this("Ongekend", "Ongekend", 1, 1, RidingStyle.UNKNOWN, LocalDate.of(2010, 1, 1));
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        if(brand == null){
            throw new IllegalArgumentException("Brand needs to have a value");
        }else{
            this.brand = brand;
        }
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        if(model == null){
            throw new IllegalArgumentException("Model needs to have a value");
        }else{
            this.model = model;
        }
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if(price <= 0){
            throw new IllegalArgumentException("Price should be above 0");
        }else{
            this.price = price;
        }
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        if(length <= 0){
            throw new IllegalArgumentException("Length should be above 0");
        }else{
            this.length = length;
        }
    }

    public RidingStyle getRidingstyle() {
        return ridingstyle;
    }

    public void setRidingstyle(RidingStyle ridingstyle) {
        if(ridingstyle == null){
            throw new IllegalArgumentException("Ridingstyle needs to have a value");
        }else{
            this.ridingstyle = ridingstyle;
        }

    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(LocalDate productionDate) {
        if(productionDate.isAfter(LocalDate.now())){
            throw new IllegalArgumentException("ProductionDate should be in the future");
        }else{
            this.productionDate = productionDate;
        }
    }

    @Override
    public boolean equals(Object o) {
        equalsCounter++;
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Snowboard snowboard = (Snowboard) o;
        return model.equals(snowboard.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(model);
    }

    @Override
    public int compareTo(Snowboard o) {
        compareCounter++;
        return this.model.compareTo(o.model);
    }

    @Override
    public String toString() {
        return String.format("Brand: %-20s Model: %-25s RidingStyle: %-18s Length (in cm): %-8d Productiondate: %-15s Prijs (euro): %.2f", brand, model, ridingstyle, length, productionDate, price);
    }
}
