package be.kdg.snowboardproject.kollections.sets;

import be.kdg.snowboardproject.kollections.Collection;
import be.kdg.snowboardproject.kollections.lists.List;

public interface Set<E> extends Collection<E> {
    List<E> toList();
}
