package be.kdg.snowboardproject.kollections.maps;

import be.kdg.snowboardproject.kollections.Collection;
import be.kdg.snowboardproject.kollections.sets.Set;

public interface Map<K, V> {
    void put(K key, V value);
    V get(K key);
    Collection<V> values();
    Set<K> keySet();
    int size();
}
