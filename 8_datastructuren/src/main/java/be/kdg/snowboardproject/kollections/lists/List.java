package be.kdg.snowboardproject.kollections.lists;

import be.kdg.snowboardproject.kollections.Collection;
import be.kdg.snowboardproject.model.Snowboard;

public interface List<T> extends Collection<T> {
    void add(int index, T element);
    void add(T element);
    void set(int index, T element);
    int size();
    T remove(int index);
    T get(int index);
    int indexOf(T element);
    boolean contains(T element);
    boolean remove (T element);
}
