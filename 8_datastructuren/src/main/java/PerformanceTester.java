import be.kdg.snowboardproject.kollections.maps.HashMap;
import be.kdg.snowboardproject.kollections.maps.ListMap;
import be.kdg.snowboardproject.kollections.maps.Map;
import be.kdg.snowboardproject.kollections.sets.ArraySet;
import be.kdg.snowboardproject.kollections.sets.Set;
import be.kdg.snowboardproject.kollections.sets.TreeSet;
import be.kdg.snowboardproject.model.Snowboard;

import be.kdg.snowboardproject.kollections.lists.ArrayList;
import be.kdg.snowboardproject.kollections.Kollections;
import be.kdg.snowboardproject.kollections.lists.LinkedList;
import be.kdg.snowboardproject.kollections.lists.List;
import be.kdg.snowboardproject.model.SnowboardFactory;

import java.util.Random;

public class PerformanceTester {
    public static List<Snowboard> randomList(int n) {
        List<Snowboard> myList = new LinkedList<>();
        new Random().ints(n).forEach(i -> myList.add(SnowboardFactory.newRandomSnowboard()));
        return myList;
    }

    public static void compareArrayListAndLinkedList(int n) {
        List<Snowboard> arrayList = new ArrayList<>();
        List<Snowboard> linkedList = new LinkedList<>();
        //add at beginning
        //arraylist
        long mil = System.currentTimeMillis();
        new Random().ints(n).forEach(i -> arrayList.add(0, SnowboardFactory.newRandomSnowboard()));
        long mil1 = System.currentTimeMillis();
        long diff1 = mil1 - mil;
        System.out.println("Adding " + n + " to ArrayList: " + diff1 + " ms.");

        //linkedlist
        long mil2 = System.currentTimeMillis();
        new Random().ints(n).forEach(i -> linkedList.add(0, SnowboardFactory.newRandomSnowboard()));
        long mil3 = System.currentTimeMillis();
        long diff2 = mil3 - mil2;
        System.out.println("Adding " + n + " to LinkedList: " + diff2 + " ms.");

        //get at end
        //arraylist
        long mil4 = System.currentTimeMillis();
        new Random().ints(n).forEach(i -> arrayList.get(n - 1));
        long mil5 = System.currentTimeMillis();
        long diff3 = mil5 - mil4;
        System.out.println("Getting " + n + " from ArrayList: " + diff3 + " ms.");

        //linkedlist
        long mil6 = System.currentTimeMillis();
        new Random().ints(n).forEach(i -> linkedList.get(n - 1));
        long mil7 = System.currentTimeMillis();
        long diff4 = mil7 - mil6;
        System.out.println("Getting " + n + " from LinkedList: " + diff4 + " ms.");
    }

    public static void testSelectionSort() {
        for (int n = 1000; n < 20000; n += 1000) {
            Kollections.selectionSort(randomList(n));
            System.out.println(n + ";" + Snowboard.compareCounter);
        }
    }

    public static void testMergeSort() {
        for (int n = 1000; n < 20000; n += 1000) {
            Kollections.mergeSort(randomList(n));
            System.out.println(n + ";" + Snowboard.compareCounter);
        }
    }


    public static void compareListMapToHashMap(int size) {
        ListMap<String, Snowboard> listMap = new ListMap<>();
        HashMap<String, Snowboard> hashMap = new HashMap<>();

        fillMap(listMap, size);
        fillMap(hashMap, size);
        long time1 = System.nanoTime();
        for (int i = 0; i < size; i++) {
            listMap.get("Snowboard" + i);
        }
        System.out.println("Listmap: n = " + size + ", equalscount = " + Snowboard.equalsCounter + ", nanosec: " + time1);

        long time2 = System.nanoTime();
        for (int i = 0; i < size; i++) {
            hashMap.get("Snowboard" + i);
        }
        System.out.println("Hashmap: n = " + size + ", equalscount = " + Snowboard.equalsCounter + ", nanosec: " + time2);
    }

    private static void fillMap(Map<String, Snowboard> map, int n) {
        Snowboard snowboard = SnowboardFactory.newEmptySnowboard();
        snowboard.setModel("Snowboard" + n);
        map.put(snowboard.getModel(), snowboard);
    }

    public static void compareArraySetToTreeSet(int n) {
        ArraySet<Snowboard> array = new ArraySet<>();
        TreeSet<Snowboard> tree = new TreeSet<>();

        Snowboard.equalsCounter = 0;
        Snowboard.compareCounter = 0;

        long time1 = System.nanoTime();
        for (int i = 0; i < n; i++) {
            fillSet(array, n);
        }
        System.out.printf("\nArrayset, n = %d: equalsCount: %d", n, Snowboard.equalsCounter);
        System.out.printf("\nArrayset, n = %d: compareCount: %d", n, Snowboard.compareCounter);
        System.out.printf("\nArrayset, n = %d: nanoSec: %d", n, System.nanoTime() - time1);

        Snowboard.equalsCounter = 0;
        Snowboard.compareCounter = 0;

        long time2 = System.nanoTime();
        for (int i = 0; i < n; i++) {
            fillSet(tree, n);
        }
        System.out.printf("\nTreeSet, n = %d: equalsCount: %d", n, Snowboard.equalsCounter);
        System.out.printf("\nTreeSet, n = %d: compareCount: %d", n, Snowboard.compareCounter);
        System.out.printf("\nTreeSet, n = %d: nanoSec: %d", n, System.nanoTime() - time2);
    }

    private static void fillSet(Set<Snowboard> set, int n) {
        for (int i = 0; i < n; i++) {
            Snowboard snowboard = SnowboardFactory.newRandomSnowboard();
            set.add(snowboard);
        }
    }
}

