import be.kdg.snowboardproject.kollections.Kollections;
import be.kdg.snowboardproject.kollections.maps.HashMap;
import be.kdg.snowboardproject.kollections.maps.Map;
import be.kdg.snowboardproject.model.RidingStyle;
import be.kdg.snowboardproject.model.Snowboard;
import be.kdg.snowboardproject.model.SnowboardFactory;
import be.kdg.snowboardproject.kollections.lists.LinkedList;
import be.kdg.snowboardproject.kollections.lists.List;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.stream.Stream;

public class Demo_8 {
    public static void main(String[] args) {

        Snowboard emptySnowboard = SnowboardFactory.newEmptySnowboard();
        System.out.println("Empty Snowboard: ");
        System.out.println(emptySnowboard.toString()  + "\n");

        Snowboard filledSnowboard = SnowboardFactory.newFilledSnowboard("BURTON", "CGTE69", 509, 160, RidingStyle.FREESTYLE, LocalDate.of(2020, 12, 8));
        System.out.println("Filled Snowboard: ");
        System.out.println(filledSnowboard.toString() + "\n");

        System.out.println("30 random snowboards gesorteerd op brand");
        Stream.generate(SnowboardFactory::newRandomSnowboard)
                .limit(30)
                .sorted(Comparator.comparing(Snowboard::getBrand))
                .forEach(System.out::println);
        System.out.println();

        System.out.println("PerformanceTester: randomList");
        List<Snowboard> snowboardList = PerformanceTester.randomList(4);
        for(int i=0; i < snowboardList.size(); i++){
            System.out.println(snowboardList.get(i));
        }
        System.out.println();

        System.out.println("Compare ArrayList and LinkedList");
        PerformanceTester.compareArrayListAndLinkedList(20000);
        System.out.println();

        System.out.println("30 random snowboards sorted (sorted on model)");
        List<Snowboard> snowboardListSorted = PerformanceTester.randomList(30);
        Kollections.selectionSort(snowboardListSorted);
        for(int i=0; i<snowboardListSorted.size(); i++){
            System.out.println(snowboardListSorted.get(i));
        }
        System.out.println();


        System.out.println("30 random snowboards sorted - mergeSort (sorted on model)");
        List<Snowboard> snowboardListMergeSorted = PerformanceTester.randomList(30);

        Kollections.mergeSort(snowboardListMergeSorted);
        for(int i=0; i<snowboardListMergeSorted.size(); i++){
            System.out.println(snowboardListMergeSorted.get(i));
        }
        System.out.println();

        /*System.out.println("Aantal compares met mergesort: ");
        PerformanceTester.testMergeSort();
        System.out.println();

        System.out.println("Aantal compares met selectionsort: ");
        PerformanceTester.testSelectionSort();
        System.out.println();*/

        System.out.println("LinearSearch & BinarySearch");
        List<Snowboard> snowboardListLB = PerformanceTester.randomList(10);
        Snowboard snowboardToSearch = snowboardListLB.get(5);
        Kollections.selectionSort(snowboardListLB);
        List<Snowboard> list2 = new LinkedList<>();
        Snowboard snowboardOnbekend = SnowboardFactory.newEmptySnowboard();
        System.out.println("Index of Snowboard " + snowboardToSearch.getModel() + ": " + Kollections.lineairSearch(snowboardListLB, snowboardToSearch));
        System.out.println("Index of Snowboard " + snowboardToSearch.getModel() + ": " + Kollections.binarySearch(snowboardListLB, snowboardToSearch));
        System.out.println("Index of Snowboard " + snowboardOnbekend.getModel() + ": " + Kollections.lineairSearch(list2, snowboardOnbekend));
        System.out.println("Index of Snowboard " + snowboardOnbekend.getModel() + ": " + Kollections.binarySearch(list2, snowboardOnbekend));
        System.out.println();

        PerformanceTester.compareListMapToHashMap(1000);

        PerformanceTester.compareArraySetToTreeSet(1000);
    }
}
