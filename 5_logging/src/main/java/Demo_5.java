import be.kdg.snowboardproject.data.Data;
import be.kdg.snowboardproject.model.RidingStyle;
import be.kdg.snowboardproject.model.Snowboard;
import be.kdg.snowboardproject.model.Snowboards;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDate;
import java.util.logging.LogManager;

public class Demo_5 {
    public static void main(String[] args) {

        URL url = Demo_5.class.getResource("/logging.properties");
        if (url != null) {
            try (InputStream input = url.openStream()) {
                LogManager.getLogManager().readConfiguration(input);
            } catch (IOException e) {
                System.err.println("Configuratiebestand is corrupt");
            }
        } else {
            System.err.println("Configuratiebestand NIET GEVONDEN");
        }

        Snowboards snowboards = new Snowboards();
        Data.getData().forEach(snowboards::add);
        snowboards.remove("CUSTOM FLYING V 166W");

        /*Snowboard snowboard1 = new Snowboard("BURTON", "egegt8856", 629.95, 166, RidingStyle.ALL_MOOUNTAIN, LocalDate.of(2021, 8, 28));
        Snowboard snowboard2 = new Snowboard("Lib tech", null, 400.6, 144, RidingStyle.JIB, LocalDate.of(2019, 9, 12));
        Snowboard snowboard3 = new Snowboard("Santa", "cxd456", 400.6, 144, RidingStyle.JIB, LocalDate.of(2025, 9, 12));
        Snowboard snowboard4 = new Snowboard("nitro", "rtf45", -400.6, 144, RidingStyle.JIB, LocalDate.of(2021, 9, 12));*/


    }
}
