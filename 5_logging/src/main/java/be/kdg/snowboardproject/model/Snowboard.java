package be.kdg.snowboardproject.model;

import java.time.LocalDate;
import java.util.Objects;
import java.util.logging.Logger;

public class Snowboard implements Comparable<Snowboard> {
    private String brand;
    private String model;
    private double price;
    private int length;
    private RidingStyle ridingstyle;
    private LocalDate productionDate;

    private final Logger logger = Logger.getLogger(Snowboard.class.getName());

    public Snowboard(String brand, String model, double price, int length, RidingStyle ridingstyle, LocalDate productionDate) {
        setBrand(brand);
        setModel(model);
        setPrice(price);
        setLength(length);
        setRidingstyle(ridingstyle);
        setProductionDate(productionDate);
    }

    public Snowboard() {
        this("Ongekend", "Ongekend", 1, 1, RidingStyle.UNKNOWN, LocalDate.of(2010, 1, 1));
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        if(brand == null){
            logger.severe(this.brand + " " + model + ": Brand needs to have a value (it cannot be " + brand + ")");
        }else{
            this.brand = brand;
        }
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        if(model == null){
            logger.severe(brand + " " + this.model + ": needs to have a value (it cannot be " + model + ")");
        }else{
            this.model = model;
        }
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if(price <= 0){
            logger.severe(brand + " " + model + ": Price should be above 0 (it cannot be " + price + ")");
        }else{
            this.price = price;
        }
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        if(length <= 0){
            logger.severe(brand + " " + model + ": Length should be above 0 (it cannot be " + length + ")");
        }else{
            this.length = length;
        }
    }

    public RidingStyle getRidingstyle() {
        return ridingstyle;
    }

    public void setRidingstyle(RidingStyle ridingstyle) {
        if(ridingstyle == null){
            logger.severe(brand + " " + model + ": Ridingstyle needs to have a value (it cannot be " + ridingstyle + ")");
        }else{
            this.ridingstyle = ridingstyle;
        }

    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(LocalDate productionDate) {
        if(productionDate.isAfter(LocalDate.now())){
            logger.severe(brand + " " + model + ": ProductionDate should be in the past (it cannot be " + productionDate + ")");
        }else{
            this.productionDate = productionDate;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Snowboard snowboard = (Snowboard) o;
        return brand.equals(snowboard.brand) &&
                model.equals(snowboard.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, model);
    }

    @Override
    public int compareTo(Snowboard o) {
        return this.brand.compareTo(o.brand) - this.model.compareTo(o.model);
    }

    @Override
    public String toString() {
        return String.format("Brand: %-20s Model: %-25s RidingStyle: %-18s Length (in cm): %-8d Productiondate: %-15s Prijs (euro): %.2f", brand, model, ridingstyle, length, productionDate, price);
    }
}
