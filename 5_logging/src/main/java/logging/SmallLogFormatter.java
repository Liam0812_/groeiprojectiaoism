package logging;

import java.text.MessageFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.util.logging.LogRecord;

public class SmallLogFormatter extends java.util.logging.Formatter{
    //voor eigen formaat logging
    @Override
    public String format(LogRecord record){
        return Instant.ofEpochMilli(record.getMillis()).atZone(ZoneId.systemDefault()).toLocalDateTime()
                + " Level: " + record.getLevel() + " melding: " + MessageFormat.format(record.getMessage(), record.getParameters()) + "\n";
    }
}
