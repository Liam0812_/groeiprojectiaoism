package be.kdg.snowboardproject.model;

public enum RidingStyle {
    ALL_MOOUNTAIN,
    FREERIDE,
    FREESTYLE,
    JIB,
    UNKNOWN
}
