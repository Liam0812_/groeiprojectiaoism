import be.kdg.snowboardproject.data.Data;
import be.kdg.snowboardproject.model.Snowboard;
import be.kdg.snowboardproject.model.Snowboards;

import java.util.List;

public class Demo_1 {
    public static void main(String[] args) {

        Snowboards snowboards = new Snowboards();
        List<Snowboard> data = Data.getData();

        data.forEach(snowboards::add);

        System.out.println("\n Snowboards sorted by Brand: ");
        snowboards.sortedOnBrand().forEach(snowboard -> System.out.println(snowboard.toString()));

        System.out.println("\n Snowboards sorted by Productiondate: ");
        snowboards.sortedOnProductionDate().forEach(snowboard -> System.out.println(snowboard.toString()));

        System.out.println("\n Snowboards sorted by Length: ");
        snowboards.sortedOnLength().forEach(snowboard -> System.out.println(snowboard.toString()));
    }
}


