package be.kdg.snowboardproject.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SnowboardsTest {
    private Snowboard snowboard1;
    private Snowboard snowboard2;
    private Snowboard snowboard3;
    private Snowboard snowboard4;
    private Snowboard snowboard5;
    private Snowboard snowboard6;

    private Snowboards snowboards;

    @BeforeEach
    void setUp() {
        snowboard1 = new Snowboard("BURTON", "CUSTOM FLYING V 166W", 629.95, 166, RidingStyle.ALL_MOOUNTAIN, LocalDate.of(2021, 8, 28));
        snowboard2 = new Snowboard("LIB TECH", "SKUNK APE 170UW", 599.95, 170, RidingStyle.ALL_MOOUNTAIN, LocalDate.of(2021, 7, 15));
        snowboard3 = new Snowboard("LIB TECH", "T.RICE APEX ORCA 156", 999.95, 156, RidingStyle.FREERIDE, LocalDate.of(2020, 5, 20));
        snowboard4 = new Snowboard("JONES", "MIND EXPANDER 158", 459.95, 158, RidingStyle.FREERIDE, LocalDate.of(2021, 1, 26));
        snowboard5 = new Snowboard("GNU", "LADIES CHOICE 148.5", 569.95, 148, RidingStyle.FREESTYLE, LocalDate.of(2021, 9, 5));
        snowboard6 = new Snowboard("CAPITA", "MEGA MERCURY 157", 739.95, 157, RidingStyle.FREERIDE, LocalDate.of(2020, 12, 9));
        snowboards = new Snowboards();

        snowboards.add(snowboard1);
        snowboards.add(snowboard2);
        snowboards.add(snowboard3);
        snowboards.add(snowboard4);
        snowboards.add(snowboard5);
        snowboards.add(snowboard6);
    }

    @Test
    public void testAdd(){
        Snowboard snowboard = new Snowboard("CAPI", "MEGA MERCURY", 739.95, 157, RidingStyle.FREERIDE, LocalDate.of(2020, 12, 9));
        assertTrue(snowboards.add(snowboard), "Toegevoegd snowboard moet uniek zijn (op brand & model)");
    }

    @Test
    public void testDelete(){
        Snowboard snowboard = new Snowboard("CAPITA", "MEGA MERCURY 157", 739.95, 157, RidingStyle.FREERIDE, LocalDate.of(2020, 12, 9));
        assertTrue(snowboards.remove(snowboard.getModel()), "Snowboard bestaat niet");
    }

    @Test
    public void testSort(){
        List<Snowboard> snowboardList = snowboards.sortedBy(Snowboard::getLength);
        assertAll(
                () -> assertEquals(snowboard1, snowboardList.get(4)),
                () -> assertEquals(snowboard2, snowboardList.get(5)),
                () -> assertEquals(snowboard3, snowboardList.get(1)),
                () -> assertEquals(snowboard4, snowboardList.get(3)),
                () -> assertEquals(snowboard5, snowboardList.get(0)),
                () -> assertEquals(snowboard6, snowboardList.get(2))
        );
    }

    @Test
    public void testSort2(){
        List<Snowboard> snowboardList = snowboards.sortedBy(Snowboard::getPrice);
        assertArrayEquals(new Snowboard[]{snowboard4, snowboard5, snowboard2, snowboard1, snowboard6, snowboard3}, snowboardList.toArray());
    }
}