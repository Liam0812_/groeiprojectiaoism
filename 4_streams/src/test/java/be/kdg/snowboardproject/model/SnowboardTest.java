package be.kdg.snowboardproject.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SnowboardTest {
    private Snowboard snowboard1;
    private Snowboard snowboard2;

    @BeforeEach
    void setUp() {
        snowboard1 = new Snowboard("Burton", "kx89", 450.99, 140, RidingStyle.FREESTYLE, LocalDate.of(2020, 5, 19));
        snowboard2 = new Snowboard("SantaCruzz", "gbp593", 270.99, 160, RidingStyle.ALL_MOOUNTAIN, LocalDate.of(2018, 1, 18));
    }

    @Test
    public void testEquals(){
        assertNotEquals(snowboard1, snowboard2, "snowboard 1 & snowboard 2 mogen niet hetzelfde zijn");

        Snowboard snowboard3 = new Snowboard("Burton", "kx89", 555.99, 150, RidingStyle.JIB, LocalDate.of(2019, 1, 18));
        assertEquals(snowboard1, snowboard3, "Snowboard 1 & 3 hebben dezelfde bepalende eigenschappen (" + snowboard1.getBrand() + " & " + snowboard1.getModel() + ")");
    }

    @Test
    public void testIllegalBrand(){
        assertThrows(IllegalArgumentException.class, () -> snowboard1.setBrand(null), "Snowboard moet een Brand value hebben!");
    }

    @Test
    public void testLegalBrand(){
        assertDoesNotThrow(() -> snowboard1.setBrand("Test"), "Snowboard moet een Brand value hebben!");
    }

    @Test
    public void testCompareTo(){
        List<Snowboard> sorted = new ArrayList<>();
        sorted.add(snowboard1);
        sorted.add(snowboard2);
        Collections.sort(sorted);

        List<Snowboard> unSorted = new ArrayList<>();
        unSorted.add(snowboard2);
        unSorted.add(snowboard1);

        assertNotEquals(unSorted, sorted);

        Snowboard snowboard3 = snowboard1;
        sorted.add(snowboard3);
        Collections.sort(sorted);
        sorted.add(snowboard3);

        assertNotEquals(sorted, unSorted);
    }

    @Test
    public void testPrijs(){
        assertEquals(snowboard1.getPrice(), 450.99);
    }


}