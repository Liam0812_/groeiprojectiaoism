import be.kdg.snowboardproject.data.Data;
import be.kdg.snowboardproject.model.RidingStyle;
import be.kdg.snowboardproject.model.Snowboard;
import be.kdg.snowboardproject.model.Snowboards;
import be.kdg.snowboardproject.util.SnowboardFunctions;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Demo_4 {
    public static void main(String[] args) {
        Snowboards snowboards = new Snowboards();
        List<Snowboard> list = Data.getData();
        list.forEach(snowboards::add);

        System.out.println("\nSnowboards gesorteerd op brand: ");
        snowboards.sortedBy(Snowboard::getBrand).forEach(System.out::println);

        System.out.println("\nSnowboards gesorteerd op prijs: ");
        snowboards.sortedBy(Snowboard::getPrice).forEach(System.out::println);

        System.out.println("\nSnowboards gesorteerd op Productie datum: ");
        snowboards.sortedBy(Snowboard::getProductionDate).forEach(System.out::println);

        //2.4
        List<Snowboard> bewerkteList;
        System.out.println("\n\ntoepassing 3x FilteredList met telkens een andere Predicate");

        System.out.println("\nAlle FREESTYLE snowboards");
        bewerkteList = SnowboardFunctions.filteredList(list, snowboard -> snowboard.getRidingstyle() == RidingStyle.FREESTYLE);
        bewerkteList.forEach(System.out::println);

        System.out.println("\nAlle snowboards langer dan 160cm");
        bewerkteList = SnowboardFunctions.filteredList(list, snowboard -> snowboard.getLength() > 160);
        bewerkteList.forEach(System.out::println);

        System.out.println("\nAlle snowboards goedkoper dan €500");
        bewerkteList = SnowboardFunctions.filteredList(list, snowboard -> snowboard.getPrice() < 500);
        bewerkteList.forEach(System.out::println);

        //2.6
        System.out.println("\nGemidelde prijs van een snowboard: " + SnowboardFunctions.average(list, Snowboard::getPrice));
        System.out.println("\nGemidelde lengte van een snowboard: " + SnowboardFunctions.average(list, Snowboard::getLength));

        //2.8
        System.out.println("\nAantal snowboard waarvan lengte korter is dan 150: " + SnowboardFunctions.countIf(list, snowboard -> snowboard.getLength() < 150));
        System.out.println("\nAantal snowboard waarvan lengte korter is dan 160: " + SnowboardFunctions.countIf(list, snowboard -> snowboard.getLength() < 160));

        //streams
        List<Snowboard> listStreams = Data.getData();

        //3.1
        System.out.println("\nAantal snowboard die na 2021 gemaakt zijn: " + SnowboardFunctions.countIf(listStreams, snowboard -> snowboard.getProductionDate().isAfter(LocalDate.of(2021,1,1))));

        //3.2
        System.out.println("\nAlle snowboards gesorteerd op brand daarna op prijs: ");
        Data.getData().stream().distinct().sorted(Comparator.comparing(Snowboard::getBrand).thenComparing(Snowboard::getPrice)).forEach(System.out::println);

        //3.3
        System.out.println("\nAlle snowboard merken (brand) in hoofdletters, omgekeerd gesorteerd en zonder dubbels: ");
        System.out.println(Data.getData().stream()
                .map(snowboard -> snowboard.getBrand().toUpperCase())
                .distinct()
                .sorted(String::compareTo)
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.joining(", "))
        );

        //3.4
        System.out.println("\nEen willekeurig snowboard met een lengte langer als 160cm: ");
        System.out.println(Data.getData().stream().filter(snowboard -> snowboard.getLength() > 160).findAny().get());

        //3.5
        System.out.println("\nDe kampioen (model) in prijs: " + Data.getData().stream().max(Comparator.comparing(Snowboard::getPrice)).map(snowboard -> snowboard.getModel()).get());
        System.out.println("\nDe kampioen (model) in lengte: " + Data.getData().stream().max(Comparator.comparing(Snowboard::getLength)).map(snowboard -> snowboard.getModel()).get());

        //3.6
        System.out.println("\nList met gesorteerde snowboard modellen die beginnen met 'M': ");
        String[] model = Data.getData().stream().distinct()
                .filter(snowboard -> snowboard.getModel().charAt(0) == 'M')
                .sorted(Comparator.comparing(Snowboard::getModel))
                .map(Snowboard::getModel)
                .toArray(String[]::new);
        System.out.println(String.join(", ", model));

        //3.7
        Map<Boolean, List<Snowboard>> map = listStreams.stream().distinct().collect(Collectors.partitioningBy(snowboard -> snowboard.getProductionDate().isBefore(LocalDate.of(2020, 1, 1))));
        List<Snowboard> voor = map.get(true);
        List<Snowboard> na = map.get(false);
        System.out.println("\nSublist van snowboards met een production date VOOR 2020");
        voor.stream().forEach(System.out::println);
        System.out.println("\nSublist van snowboards met een production date NA 2020");
        na.stream().forEach(System.out::println);

        //3.8
        System.out.println("\nAlle snowboards gegroepeerd per RidingStyle");
        Map<RidingStyle, List <Snowboard>> snowboardmap = listStreams.stream().distinct().collect(Collectors.groupingBy(Snowboard::getRidingstyle));
        snowboardmap.forEach((ridingstyle, snowboard) -> System.out.printf("%8s%-20s: %s\n", "", ridingstyle, snowboard.stream().map(Snowboard::getModel).sorted().collect(Collectors.joining(","))));

    }
}
