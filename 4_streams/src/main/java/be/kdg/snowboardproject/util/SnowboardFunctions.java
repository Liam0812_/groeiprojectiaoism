package be.kdg.snowboardproject.util;

import be.kdg.snowboardproject.model.Snowboard;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;

public class SnowboardFunctions {
    public static <T> List<T> filteredList(List<T> snowboardList, Predicate<T> predicate){
        List<T> list = new LinkedList<>();
        snowboardList.stream().filter(predicate::test).forEach(list::add);
        return list;
    }

    public static <T> Double average(List<T> snowboardList, ToDoubleFunction<T> mapper){
        double average = 0;
        double sum = snowboardList.stream().mapToDouble(mapper::applyAsDouble).sum();
        average = sum / snowboardList.size();
        return average;
    }

    public static <T> long countIf(List<T> snowboardList, Predicate<T> predicate){
        long count = snowboardList.stream().filter(predicate::test).count();
        return count;
    }


}
