package be.kdg.snowboardproject;

import be.kdg.snowboardproject.database.SnowboardDbDao;
import be.kdg.snowboardproject.service.SnowboardService;
import be.kdg.snowboardproject.service.SnowboardServiceImpl;
import be.kdg.snowboardproject.view.SnowboardsPresenter;
import be.kdg.snowboardproject.view.SnowboardsView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        SnowboardDbDao snowboardDbDao = SnowboardDbDao.getInstance("db/games");
        SnowboardsView view = new SnowboardsView();
        SnowboardService service = new SnowboardServiceImpl(snowboardDbDao);
        new SnowboardsPresenter(service, view);

        primaryStage.setScene(new Scene(view));
        primaryStage.show();
    }
}
