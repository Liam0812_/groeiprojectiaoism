package be.kdg.snowboardproject.view;

import be.kdg.snowboardproject.model.Snowboard;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

import java.time.LocalDate;
import java.util.ArrayList;

public class SnowboardsView extends BorderPane{
    private TableView tableView;
    private TextField brandField;
    private TextField lengthField;
    private DatePicker datePicker;
    private Button btn_save;

    public SnowboardsView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes(){
        tableView = new TableView<>();
        brandField = new TextField();
        lengthField = new TextField();
        datePicker = new DatePicker();
        btn_save = new Button("Save");
    }

    private void layoutNodes(){
        setCenter(tableView);
        TableColumn<Snowboard, String> brand = new TableColumn<>("Brand");
        brand.setCellValueFactory(new PropertyValueFactory<>("Brand"));
        TableColumn<Snowboard, Integer> length = new TableColumn<>("Length");
        length.setCellValueFactory(new PropertyValueFactory<>("Length"));
        TableColumn<Snowboard, LocalDate> date = new TableColumn<>("ProductionDate");
        date.setCellValueFactory(new PropertyValueFactory<>("ProductionDate"));

        tableView.getColumns().addAll(brand, length, date);
        setCenter(tableView);
        BorderPane.setMargin(tableView, new javafx.geometry.Insets(10));

        HBox box = new HBox(brandField, lengthField, datePicker, btn_save);
        BorderPane.setMargin(box, new javafx.geometry.Insets(10));
        box.setSpacing(5);
        setBottom(box);
        brandField.setPromptText("Brand");
        lengthField.setPromptText("Length");
        datePicker.setPromptText("ProductionDate");
    }

    TableView getTableView() {
        return tableView;
    }

    TextField getBrandField() {
        return brandField;
    }

    TextField getLengthField() {
        return lengthField;
    }

    DatePicker getDatePicker() {
        return datePicker;
    }

    Button getBtn_save() {
        return btn_save;
    }
}
