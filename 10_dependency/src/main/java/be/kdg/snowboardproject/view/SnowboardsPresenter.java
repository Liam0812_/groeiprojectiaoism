package be.kdg.snowboardproject.view;


import be.kdg.snowboardproject.exceptions.SnowboardException;
import be.kdg.snowboardproject.model.Snowboard;
import be.kdg.snowboardproject.service.SnowboardService;
import javafx.collections.FXCollections;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.sql.SQLException;

public class SnowboardsPresenter {
    SnowboardService service;
    SnowboardsView view;

    public SnowboardsPresenter(SnowboardService service, SnowboardsView view) {
        this.view = view;
        this.service = service;
        this.addEventHandlers();
        this.setSnowboard();
    }

    private void addEventHandlers() {
        view.getBtn_save().setOnAction(click -> {
            try{
                Snowboard snowboard = new Snowboard(view.getBrandField().getText(), Integer.parseInt(view.getLengthField().getText()), view.getDatePicker().getValue());
                service.addSnowboard(snowboard);
            }
            catch(IllegalArgumentException | SnowboardException e){
                new Alert(Alert.AlertType.WARNING, e.getMessage()).showAndWait();
            }
            setSnowboard();
        });
    }

    private void setSnowboard(){
        try{
            view.getTableView().setItems(FXCollections.observableList(service.getAllSnowboard()));
        }catch(IllegalArgumentException | SnowboardException e){
            new Alert(Alert.AlertType.WARNING, e.getMessage()).showAndWait();
        }
    }
}
