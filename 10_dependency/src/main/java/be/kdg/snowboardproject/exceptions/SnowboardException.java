package be.kdg.snowboardproject.exceptions;

public class SnowboardException extends RuntimeException {

    public SnowboardException(){

    }

    public SnowboardException(String message){
        super(message);
    }

    public SnowboardException(String message, Throwable cause){
        super(message, cause);
    }

    public SnowboardException(Throwable cause){
        super(cause);
    }
}
