package be.kdg.snowboardproject.database;

import be.kdg.snowboardproject.model.Snowboard;

import java.util.List;

public interface SnowboardDao {
    boolean insert(Snowboard snowboard);
    boolean delete(String naam);
    boolean update(Snowboard snowboard);
    Snowboard retrieve(String naam);
    List<Snowboard> sortedOn(String query);
    List<Snowboard> getAllSnowboards();
}
