package be.kdg.snowboardproject.database;

import be.kdg.snowboardproject.data.Data;
import be.kdg.snowboardproject.exceptions.SnowboardException;
import be.kdg.snowboardproject.model.RidingStyle;
import be.kdg.snowboardproject.model.Snowboard;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class SnowboardDbDao implements SnowboardDao {
    private static Connection connection;
    private static Logger loging = Logger.getLogger(SnowboardDbDao.class.getName());

    public SnowboardDbDao() {
    }

    public static SnowboardDbDao getInstance(String path) {
        if(connection == null){
            try{
                connection = DriverManager.getConnection("jdbc:hsqldb:file:"+path, "sa", "");
                createTable();
            }
            catch(SQLException throwable){
                loging.warning(throwable.getMessage());
                throw new SnowboardException(throwable);
            }
        }
        return new SnowboardDbDao();
    }

    public void close(){
        if(connection != null){
            try{
                connection.close();
            }
            catch(SQLException e){
                loging.warning(e.getMessage());
                throw new SnowboardException(e);
            }
        }
    }

    private static void createTable(){
        try{
            DatabaseMetaData metaDataDb = connection.getMetaData();
            ResultSet existingTables = metaDataDb.getTables(null, null, "snowboards", null);
            if(!existingTables.next()){
                Statement statement = connection.createStatement();
                statement.execute("DROP TABLE snowboards IF EXISTS");
                String query = "CREATE TABLE snowboards (" +
                        "id INTEGER IDENTITY," +
                        "brand VARCHAR(15)," +
                        "model VARCHAR(25)," +
                        "price DECIMAL(4,0)," +
                        "length INTEGER," +
                        "ridingstyle VARCHAR(27)," +
                        "productiondate DATE)";
                statement.execute(query);
                statement.close();
                Data.getData().forEach(new SnowboardDbDao()::insert);
                loging.info("Tabel aangemaakt en gevuld met data");
            }

        }
        catch(SQLException e){
            loging.warning(e.getMessage());
            throw new SnowboardException(e);
        }
    }

    @Override
    public boolean insert(Snowboard snowboard) {
        try{
            String statement = "INSERT INTO snowboards(id, brand, model, price, length, ridingstyle, productiondate) VALUES(null,?,?,?,?,?,?)";
            PreparedStatement prep = connection.prepareStatement(statement);
            prep.setString(1, snowboard.getBrand());
            prep.setString(2, snowboard.getModel());
            prep.setDouble(3, snowboard.getPrice());
            prep.setInt(4, snowboard.getLength());
            prep.setString(5, snowboard.getRidingstyle().name());
            prep.setDate(6, Date.valueOf(snowboard.getProductionDate()));

            boolean inserted = prep.execute();
            prep.close();
            return !inserted;
        }
        catch(SQLException e){
            loging.warning(e.getMessage());
            throw new SnowboardException(e);
        }
    }

    @Override
    public boolean delete(String naamModel) {
        String statement;
        PreparedStatement prep;
        try{
            if(naamModel.equals("*")){
                statement = "TRUNCATE TABLE snowboards";
                prep = connection.prepareStatement(statement);
            }
            else{
               statement = "DELETE FROM  snowboards WHERE model = ?";
               prep = connection.prepareStatement(statement);
               prep.setString(1, naamModel);
            }
            boolean deleted = prep.executeUpdate() > 0;
            prep.close();
            return deleted;
        }
        catch(SQLException e){
            loging.warning(e.getMessage());
            throw new SnowboardException(e);
        }
    }

    @Override
    public boolean update(Snowboard snowboard) {
        try{
            String statement = "UPDATE snowboards SET "+
                    "brand = ?, "+
                    "model = ?, "+
                    "price = ?, "+
                    "length = ?, "+
                    "ridingstyle = ?, "+
                    "productiondate = ? "+
                    "WHERE id = ?";
            PreparedStatement prep = connection.prepareStatement(statement);
            prep.setString(1, snowboard.getBrand());
            prep.setString(2, snowboard.getModel());
            prep.setDouble(3, snowboard.getPrice());
            prep.setInt(4, snowboard.getLength());
            prep.setString(5, snowboard.getRidingstyle().name());
            prep.setDate(6, Date.valueOf(snowboard.getProductionDate()));
            prep.setInt(7, snowboard.getId());

            boolean updated = prep.executeUpdate() > 0;
            prep.close();
            return updated;
        }
        catch(SQLException e){
            loging.warning(e.getMessage());
            throw new SnowboardException(e);
        }
    }

    @Override
    public Snowboard retrieve(String modelName) {
        try{
            String statement = "SELECT * FROM snowboards WHERE model = ? LIMIT 1";
            PreparedStatement prep = connection.prepareStatement(statement);
            prep.setString(1, modelName);
            ResultSet resultSet = prep.executeQuery();

            Snowboard snowboard;
            if(resultSet.next()){
                int id = resultSet.getInt("id");
                String brand = resultSet.getString("brand");
                String model = resultSet.getString("model");
                double price = resultSet.getDouble("price");
                int length = resultSet.getInt("length");
                RidingStyle ridingStyle = RidingStyle.valueOf(resultSet.getString("ridingstyle"));
                LocalDate date = resultSet.getDate("productiondate").toLocalDate();

                snowboard = new Snowboard(brand, model, price, length, ridingStyle, date, id);
            }else{
                snowboard = null;
            }
            prep.close();
            resultSet.close();
            return snowboard;
        }
        catch(SQLException e){
            loging.warning(e.getMessage());
            throw new SnowboardException(e);
        }
    }

    @Override
    public List<Snowboard> sortedOn(String query) {
        try{
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            List<Snowboard> snowboards = new ArrayList<>();
            while(resultSet.next()){
                snowboards.add(new Snowboard(
                        resultSet.getString("brand"),
                        resultSet.getString("model"),
                        resultSet.getDouble("price"),
                        resultSet.getInt("length"),
                        RidingStyle.valueOf(resultSet.getString("ridingstyle")),
                        resultSet.getDate("productiondate").toLocalDate(),
                        resultSet.getInt("id")
                        ));
            }
            statement.close();
            resultSet.close();
            return snowboards;
        }
        catch(SQLException e){
            loging.warning(e.getMessage());
            throw new SnowboardException(e);
        }
    }

    public List<Snowboard> sortedOnBrand(){
        return sortedOn("SELECT * FROM snowboards ORDER BY brand");
    }

    public List<Snowboard> sortedOnModel(){
        return sortedOn("SELECT * FROM snowboards ORDER BY model");
    }

    public List<Snowboard> sortedOnPrice(){
        return sortedOn("SELECT * FROM snowboards ORDER BY price");
    }

    public List<Snowboard> sortedOnDate(){
        return sortedOn("SELECT * FROM snowboards ORDER BY productiondate");
    }

    @Override
    public List<Snowboard> getAllSnowboards() {
        return sortedOnBrand();
    }
}
