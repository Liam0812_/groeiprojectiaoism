package be.kdg.snowboardproject.data;

import be.kdg.snowboardproject.model.RidingStyle;
import be.kdg.snowboardproject.model.Snowboard;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

//De production date van een snowboard is willekeurig gekozen

public class Data {
    public static List<Snowboard> snowboards = new ArrayList<Snowboard>();

    public static List<Snowboard> getData(){
        snowboards.add(new Snowboard("BURTON", "CUSTOM FLYING V 166W", 629.95, 166, RidingStyle.ALL_MOOUNTAIN, LocalDate.of(2021, 8, 28)));
        snowboards.add(new Snowboard("LIB TECH", "SKUNK APE 170UW", 599.95, 170, RidingStyle.ALL_MOOUNTAIN, LocalDate.of(2021, 7, 15)));
        snowboards.add(new Snowboard("LIB TECH", "T.RICE APEX ORCA 156", 999.95, 156, RidingStyle.FREERIDE, LocalDate.of(2020, 5, 20)));
        snowboards.add(new Snowboard("JONES", "MIND EXPANDER 158", 599.95, 158, RidingStyle.FREERIDE, LocalDate.of(2021,1, 26)));
        snowboards.add(new Snowboard("GNU", "LADIES CHOICE 148.5", 569.95, 148, RidingStyle.FREESTYLE, LocalDate.of(2021, 9, 5)));
        snowboards.add(new Snowboard("CAPITA", "MEGA MERCURY 157", 739.95, 157, RidingStyle.FREERIDE, LocalDate.of(2020, 12, 8)));
        snowboards.add(new Snowboard("NITRO", "TEAM WIDE 159", 499.95, 159, RidingStyle.ALL_MOOUNTAIN, LocalDate.of(2020, 11, 30)));
        snowboards.add(new Snowboard("BURTON", "YEASAYER 148", 479.95, 148, RidingStyle.ALL_MOOUNTAIN, LocalDate.of(2021, 10, 29)));
        snowboards.add(new Snowboard("LIB TECH", "BOX KNIFE 160W", 499.95, 160, RidingStyle.JIB, LocalDate.of(2019, 12, 30)));
        snowboards.add(new Snowboard("NEVER SUMMER", "SLINGER 157W", 679.95, 157, RidingStyle.FREESTYLE, LocalDate.of(2019, 10, 8)));
        snowboards.add(new Snowboard("GNU", "BANKED COUNTRY 160W", 529.95, 160, RidingStyle.FREERIDE, LocalDate.of(2021, 1, 1)));
        snowboards.add(new Snowboard("SANTA CRUZ", "JP WALKER PRO 154", 549.95, 154, RidingStyle.ALL_MOOUNTAIN, LocalDate.of(2019, 12, 8)));
        snowboards.add(new Snowboard("ALIBI", "BLOOM 151", 279.95, 151, RidingStyle.ALL_MOOUNTAIN, LocalDate.of(2019, 11, 30)));
        snowboards.add(new Snowboard("K2", "MANIFEST TEAM 159", 574.95, 159, RidingStyle.FREERIDE, LocalDate.of(2019, 9, 14)));
        snowboards.add(new Snowboard("SANTA CRUZ", "SPILL HAND 163W", 498.95, 163, RidingStyle.FREESTYLE, LocalDate.of(2019, 5, 10)));
        snowboards.add(new Snowboard("RIDE", "PSYCHOCANDY 154", 514.95, 154, RidingStyle.FREERIDE, LocalDate.of(2019, 7, 19)));
        return snowboards;

    }
}
