package be.kdg.snowboardproject.service;

import be.kdg.snowboardproject.database.SnowboardDao;
import be.kdg.snowboardproject.database.SnowboardDbDao;
import be.kdg.snowboardproject.exceptions.SnowboardException;
import be.kdg.snowboardproject.model.Snowboard;

import java.util.List;

public class SnowboardServiceImpl implements SnowboardService{
    private final SnowboardDbDao snowboardDbDao;

    public SnowboardServiceImpl(SnowboardDao snowboardDao) {
        snowboardDbDao = SnowboardDbDao.getInstance("db/snowboards");
    }

    @Override
    public List<Snowboard> getAllSnowboard() throws SnowboardException {
        return snowboardDbDao.getAllSnowboards();
    }

    @Override
    public boolean addSnowboard(Snowboard snowboard) throws SnowboardException{
        return snowboardDbDao.insert(snowboard);
    }
}
