package be.kdg.snowboardproject.service;

import be.kdg.snowboardproject.model.Snowboard;

import java.util.List;

public interface SnowboardService {
    List<Snowboard> getAllSnowboard();
    boolean addSnowboard(Snowboard snowboard);
}
