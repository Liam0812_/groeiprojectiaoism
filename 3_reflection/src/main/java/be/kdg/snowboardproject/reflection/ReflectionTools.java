package be.kdg.snowboardproject.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.List;

public class ReflectionTools {
    public static void classAnalysis(Class aClass){
        System.out.printf("\nAnalyse van de klasse: %s\n", aClass.getSimpleName());
        System.out.println("==================================================");

        System.out.printf("Fully qualified name: %s\n", aClass.getSimpleName());

        System.out.printf("Naam superklasse: %s\n", aClass.getSuperclass());

        System.out.printf("Naam van de package: %s\n", aClass.getPackageName());

        System.out.print("Interfaces: ");
        for(Class interfaceNaam : aClass.getInterfaces()) {
            System.out.print(interfaceNaam.getSimpleName());
        }

        System.out.print("\nConstructors: ");
        for(Constructor constructorNaam: aClass.getDeclaredConstructors()){
            System.out.print(constructorNaam);
        }

        System.out.print("\nAttributen: ");
        for(Field attribuut: aClass.getDeclaredFields()){
            attribuut.setAccessible(true); //toegang tot private attributen
            System.out.print(attribuut.getName() + "(" + attribuut.getType() +")  ");
        }

        System.out.print("\nGetters: ");
        for(Method method: aClass.getDeclaredMethods()){
            if(method.getName().startsWith("get")){
                System.out.print(method.getName() + " ");
            }
        }

        System.out.print("\nSetters: ");
        for(Method method: aClass.getDeclaredMethods()){
            if(method.getName().startsWith("set")){
                System.out.print(method.getName() + " ");
            }
        }

        System.out.print("\nAndere methoden: ");
        for(Method method: aClass.getDeclaredMethods()){
            if(!(method.getName().startsWith("get") || method.getName().startsWith("set"))){
                System.out.print(method.getName() + " ");
            }
        }
        System.out.println();


    }

    public static Object runAnnotated(Class aClass){
        try{
            Object obj = aClass.getDeclaredConstructor().newInstance();
            for(Method method : aClass.getMethods()){
                CanRun canRun = method.getAnnotation(CanRun.class);
                Type[] parameterType = method.getGenericParameterTypes();
                if(parameterType.length == 1 && canRun != null && parameterType[0].getTypeName().equals("java.lang.String")){
                    method.invoke(obj, canRun.value());
                }
            }
            return obj;
        }
        catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
