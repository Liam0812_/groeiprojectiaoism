package be.kdg.snowboardproject.reflection;

public @interface CanRun {
    String value() default "dummy";
}
