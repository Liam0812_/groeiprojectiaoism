package be.kdg.snowboardproject.model;

import java.time.LocalDate;
import java.util.Objects;

public class Snowboard extends be.kdg.snowboardproject.model.Board {
    private int length;
    private RidingStyle ridingstyle;
    private LocalDate productionDate;

    public Snowboard(String brand, String model, double price, int length, RidingStyle ridingstyle, LocalDate productionDate) {
        super(brand, model, price);
        setLength(length);
        setRidingstyle(ridingstyle);
        setProductionDate(productionDate);
    }

    public Snowboard() {
        this("Ongekend", "Ongekend", 1, 1, RidingStyle.UNKNOWN, LocalDate.of(2010, 1, 1));
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        if(length <= 0){
            throw new IllegalArgumentException("Length should be above 0");
        }else{
            this.length = length;
        }
    }

    public RidingStyle getRidingstyle() {
        return ridingstyle;
    }

    public void setRidingstyle(RidingStyle ridingstyle) {
        if(ridingstyle == null){
            throw new IllegalArgumentException("Ridingstyle needs to have a value");
        }else{
            this.ridingstyle = ridingstyle;
        }

    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(LocalDate productionDate) {
        if(productionDate.isAfter(LocalDate.now())){
            throw new IllegalArgumentException("ProductionDate should be in the past");
        }else{
            this.productionDate = productionDate;
        }
    }

    @Override
    public String toString() {
        //return String.format("Brand: %-20s Model: %-25s RidingStyle: %-18s Length (in cm): %-8d Productiondate: %-15s Prijs (euro): %.2f", super.brand, super.model, ridingstyle, length, productionDate, super.price);
        return super.toString(getLength(), getRidingstyle(), getProductionDate());
    }
}
