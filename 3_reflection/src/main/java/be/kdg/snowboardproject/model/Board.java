package be.kdg.snowboardproject.model;

import be.kdg.snowboardproject.model.Snowboard;
import be.kdg.snowboardproject.reflection.CanRun;

import java.time.LocalDate;
import java.util.Objects;

public class Board implements Comparable<Snowboard> {
    protected String brand;
    protected String model;
    protected double price;

    public Board(String brand, String model, double price) {
        this.brand = brand;
        this.model = model;
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    @CanRun("Burton")
    public void setBrand(String brand) {
        if (brand == null) {
            throw new IllegalArgumentException("Brand needs to have a value");
        } else {
            this.brand = brand;
        }
    }

    public String getModel() {
        return model;
    }

    @CanRun() //geen value-parameter -> default
    public void setModel(String model) {
        if (model == null) {
            throw new IllegalArgumentException("Model needs to have a value");
        } else {
            this.model = model;
        }
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if (price <= 0) {
            throw new IllegalArgumentException("Price should be above 0");
        } else {
            this.price = price;
        }
    }

    @Override
    public int compareTo(Snowboard o) {
        return this.brand.compareTo(o.brand) - this.model.compareTo(o.model);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Board board = (Board) o;
        return brand.equals(board.brand) &&
                model.equals(board.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, model);
    }

    public String toString(int length, RidingStyle ridingStyle, LocalDate productionDate) {
        return String.format("Brand: %-20s Model: %-25s RidingStyle: %-18s Length (in cm): %-8d Productiondate: %-15s Prijs (euro): %.2f", brand, model, ridingStyle, length, productionDate, price);
    }
}
