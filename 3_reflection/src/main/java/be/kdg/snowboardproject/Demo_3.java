package be.kdg.snowboardproject;

import be.kdg.snowboardproject.model.Snowboard;
import be.kdg.snowboardproject.model.Snowboards;
import be.kdg.snowboardproject.reflection.ReflectionTools;
import be.kdg.snowboardproject.model.Board;

public class Demo_3 {
    public static void main(String[] args) {
        ReflectionTools.classAnalysis(Board.class);
        ReflectionTools.classAnalysis(Snowboard.class);
        ReflectionTools.classAnalysis(Snowboards.class);

        System.out.println();
        System.out.printf("Aangemaakt object door runAnnotated:\n %s", ReflectionTools.runAnnotated(Snowboard.class));
    }
}
