package be.kdg.snowboardproject.model;

import java.time.LocalDate;
import java.util.Objects;

/**
 * @author Liam Smet
 * @version 1.0
 * @see <a href="https://www.blue-tomato.com/nl-NL/"> Snowboards </a>
 *
 * In this class are all the attributes (with getters and setters) for a snowboard
 *
 */

public class Snowboard implements Comparable<Snowboard> {
    /**
     * the snowboard brand
     */
    private String brand;

    /**
     * the snowboard model
     */
    private String model;

    /**
     * the price of a snowboard
     */
    private double price;

    /**
     * the length of a snowboard
     */
    private int length;

    /**
     * the ridingstyle of a snowboard
     */
    private RidingStyle ridingstyle;

    /**
     * the production date of a snowboard
     */
    private LocalDate productionDate;

    /**
     *
     * @param brand the snowboard brand
     * @param model the snowboard model
     * @param price the price of a snowboard
     * @param length the length of a snowboard
     * @param ridingstyle the ridingstyle of a snowboard
     * @param productionDate the productiondate of a snowboard
     */
    public Snowboard(String brand, String model, double price, int length, RidingStyle ridingstyle, LocalDate productionDate) {
        setBrand(brand);
        setModel(model);
        setPrice(price);
        setLength(length);
        setRidingstyle(ridingstyle);
        setProductionDate(productionDate);
    }

    public Snowboard() {
        this("Ongekend", "Ongekend", 1, 1, RidingStyle.UNKNOWN, LocalDate.of(2010, 1, 1));
    }

    /**
     *
     * @return the brand of a snowbaord
     */
    public String getBrand() {
        return brand;
    }

    /**
     *
     * @param brand the brand of a snowboard
     */
    public void setBrand(String brand) {
        if(brand == null){
            throw new IllegalArgumentException("Brand needs to have a value");
        }else{
            this.brand = brand;
        }
    }

    /**
     *
     * @return the model of a snowboard
     */
    public String getModel() {
        return model;
    }

    /**
     *
     * @param model the model of a snowboard
     */
    public void setModel(String model) {
        if(model == null){
            throw new IllegalArgumentException("Model needs to have a value");
        }else{
            this.model = model;
        }
    }

    /**
     *
     * @return the price of a snowboard
     */
    public double getPrice() {
        return price;
    }

    /**
     *
     * @param price the price of a snowboard
     */
    public void setPrice(double price) {
        if(price <= 0){
            throw new IllegalArgumentException("Price should be above 0");
        }else{
            this.price = price;
        }
    }

    /**
     *
     * @return the length of a snowboard
     */
    public int getLength() {
        return length;
    }

    /**
     *
     * @param length the length of a snowboard
     */
    public void setLength(int length) {
        if(length <= 0){
            throw new IllegalArgumentException("Length should be above 0");
        }else{
            this.length = length;
        }
    }

    /**
     *
     * @return ridingstyle of a snowboard
     * @see be.kdg.snowboardproject.model.RidingStyle
     */
    public RidingStyle getRidingstyle() {
        return ridingstyle;
    }

    /**
     *
     * @param ridingstyle of a snowboard
     * @see be.kdg.snowboardproject.model.RidingStyle
     */
    public void setRidingstyle(RidingStyle ridingstyle) {
        if(ridingstyle == null){
            throw new IllegalArgumentException("Ridingstyle needs to have a value");
        }else{
            this.ridingstyle = ridingstyle;
        }

    }

    /**
     *
     * @return the productiondate of a snowboard
     */
    public LocalDate getProductionDate() {
        return productionDate;
    }

    /**
     *
     * @param productionDate the productiondate of a snowboard
     */
    public void setProductionDate(LocalDate productionDate) {
        if(productionDate.isAfter(LocalDate.now())){
            throw new IllegalArgumentException("ProductionDate should be in the past");
        }else{
            this.productionDate = productionDate;
        }
    }

    /**
     *
     * @param o an object (in this case a snowboard)
     * @return a boolean gives true or false
    */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Snowboard snowboard = (Snowboard) o;
        return brand.equals(snowboard.brand) &&
                model.equals(snowboard.model);
    }

    /**
     *
     * @return gives a hashcode of an object
    */
    @Override
    public int hashCode() {
        return Objects.hash(brand, model);
    }

    /**
     *
     * @param o a snowboardo object
     * @return a number: negative int: less than, zero: equal to, positive int: greater than
    */
    @Override
    public int compareTo(Snowboard o) {
        return this.brand.compareTo(o.brand) - this.model.compareTo(o.model);
    }

    /**
     *
     * @return a String
     */
    @Override
    public String toString() {
        return String.format("Brand: %-20s Model: %-25s RidingStyle: %-18s Length (in cm): %-8d Productiondate: %-15s Prijs (euro): %.2f", brand, model, ridingstyle, length, productionDate, price);
    }
}
