package be.kdg.snowboardproject.generics;

import be.kdg.snowboardproject.model.Snowboard;

import java.util.*;

public class PriorityQueue<T> implements FIFOQueue<T> {

    private Map<Integer, LinkedList<T>> map = new TreeMap<>(Collections.reverseOrder());

    /**
     * @param element - element that needs to be set in the queue (in this case a snowboard)
     * @param priority - the priority of that element (in this case a snowboard)
     * @return boolean - gives true/false
     */
    @Override
    public boolean enqueue(T element, int priority) {
        for(LinkedList<T> list : map.values()){
            for(T e: list){
                if(element == e){
                    return false;
                }
            }
        }
        map.putIfAbsent(priority, new LinkedList<T>());
        map.get(priority).addLast((T) element);
        return true;
    }

    /**
     *
     * @return the value that needs to be dequeued (in this case a snowboard)
     */
    @Override
    public T dequeue() {
        T element = null;
        for(int i=5; i>0; i--){
            if(map.containsKey(i)) {
                LinkedList<T> first = map.get(i);
                if (first.size() != 0) {
                    element = first.getFirst();
                    first.remove(element);
                    break;
                }
            }
        }
        return element;
    }

    /**
     *
     * @param element - a element that needs to be searched (in this case a snowboard)
     * @return the position of that element/object (snowboard) in the queue
     */
    @Override
    public int search(Object element) {
        Iterator<Map.Entry<Integer, LinkedList<T>>> it = map.entrySet().iterator();
        int index = 0;
        while(it.hasNext()){
            for(T lijst: it.next().getValue()){
                index++;
                if(lijst.equals(element)){
                    return index;
                }
            }
        }
        return -1;
    }

    /**
     *
     * @return the size of the queue
     */
    @Override
    public int getSize() {
        return map.values().stream().mapToInt(Collection::size).sum();
    }

    /**
     *
     * @return a string
     */
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        Iterator<Map.Entry<Integer, LinkedList<T>>> it = map.entrySet().iterator();
        Map.Entry<Integer, LinkedList<T>> entry = null;
        while(it.hasNext()){
            entry = it.next();
            for (T lijst: entry.getValue()) {
                str.append(entry.getKey() + " : " + lijst + "\n");
            }
        }
        return str.toString();
    }
}
