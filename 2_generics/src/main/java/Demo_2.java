import be.kdg.snowboardproject.data.Data;
import be.kdg.snowboardproject.generics.PriorityQueue;
import be.kdg.snowboardproject.model.Snowboard;

import java.util.List;
import java.util.Random;

/**
 * @author Liam Smet
 * @version 1.0
 * @see <a href="https://www.blue-tomato.com/nl-NL/"> Snowboards </a>
 *
 * This is the class with a main thats make the application runnable
 *
 */

public class Demo_2 {
    public static void main(String[] args) {

        /*
        PriorityQueue<String> myQeueu = new PriorityQueue<>();

        myQeueu.enqueue("Tokio", 2);
        myQeueu.enqueue("Denver", 5);
        myQeueu.enqueue("Rio", 2);
        myQeueu.enqueue("Oslo", 3);

        System.out.println("overzicht v/d PriorityQueue: ");
        System.out.println(myQeueu.toString());
        System.out.println("aantal: " + myQeueu.getSize());
        System.out.println("positie van Tokio: " + myQeueu.search("Tokio"));
        System.out.println("positie van Nairobi: " + myQeueu.search("Nairobi"));

        for(int i=0; i<4; i++){
            System.out.println("Dequeue: " + myQeueu.dequeue());
        }
        System.out.println("size na dequeue: " + myQeueu.getSize());
        */

        var queue = new PriorityQueue<Snowboard>();
        var data = Data.getData();
        var rand = new Random();

        for(Snowboard board: data){
            queue.enqueue(board, rand.nextInt(5)+1);
        }

        System.out.println("huidige PriorityQueue: ");
        System.out.println(queue.toString());

        System.out.println("aantal: " + queue.getSize());
        System.out.println();

        System.out.println("positie van K2: " + queue.search("K2"));

        for(int i=0; i< data.size(); i++){
            System.out.println("Dequeue: " + queue.dequeue());
        }
        System.out.println("Size na dequeue: " + queue.getSize());
    }
}
