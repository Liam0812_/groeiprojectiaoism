package be.kdg.snowboardproject.persist;

import be.kdg.snowboardproject.model.Snowboard;
import be.kdg.snowboardproject.model.Snowboards;

import java.io.*;

public class SnowboardSerializer {

    private final String FILENAME;

    public SnowboardSerializer(String FILENAME) {
        this.FILENAME = FILENAME;
    }

    public void serialize(Snowboards snowboard) throws IOException{
        ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("./db/snowboards.ser"));
        outputStream.writeObject(snowboard);
    }

    public Snowboards deserialize() throws IOException, ClassNotFoundException{
        FileInputStream fileInputStream = new FileInputStream("./db/snowboards.ser");
        ObjectInput objectInput = new ObjectInputStream(fileInputStream);
        Snowboards snowboard = (Snowboards) objectInput.readObject();
        objectInput.close();
        fileInputStream.close();
        return snowboard;
    }
}
