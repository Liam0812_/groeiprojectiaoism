package be.kdg.snowboardproject.model;

import java.io.Serializable;
import java.util.*;

public class Snowboards implements Serializable {
    private Set<Snowboard> snowboardSet = new TreeSet<>();
    boolean added;
    boolean removed;

    private static final long serialVersionUID = 1L;

    public boolean add (Snowboard snowboard){
        added = snowboardSet.add(snowboard);
        return added;
    }

    public boolean remove(String model){
        Iterator<Snowboard> it = snowboardSet.iterator();
        while(it.hasNext()){
            Snowboard snowboard = it.next();
            if(snowboard.getModel().equals(model)){
                it.remove();
                removed = true;
            }
        }
        return removed;
    }

    public Snowboard search(String model){
        Snowboard searchedBoard = new Snowboard();
        for(Snowboard snowboard: snowboardSet){
            if(snowboard.getModel().equals(model)){
                searchedBoard = snowboard;
            }
        }
        return searchedBoard;
    }

    public List<Snowboard> sortedOnBrand(){
        List<Snowboard> snowboards = new ArrayList<>();
        snowboards.addAll(snowboardSet);

        Collections.sort(snowboards, new Comparator<Snowboard>() {
            @Override
            public int compare(Snowboard o1, Snowboard o2) {
                return o1.getBrand().compareTo(o2.getBrand());
            }
        });
        return snowboards;
    }

    public List<Snowboard> sortedOnProductionDate(){
        List<Snowboard> snowboards = new ArrayList<>();
        snowboards.addAll(snowboardSet);

        Collections.sort(snowboards, new Comparator<Snowboard>() {
            @Override
            public int compare(Snowboard o1, Snowboard o2) {
                return o1.getProductionDate().compareTo(o2.getProductionDate());
            }
        });
        return snowboards;
    }

    public List<Snowboard> sortedOnLength(){
        List<Snowboard> snowboards = new ArrayList<>();
        snowboards.addAll(snowboardSet);

        Collections.sort(snowboards, new Comparator<Snowboard>() {
            @Override
            public int compare(Snowboard o1, Snowboard o2) {
                return o1.getLength() - o2.getLength();
            }
        });
        return snowboards;
    }

    public int getSize(){
        return snowboardSet.size();
    }

    public Set<Snowboard> getSnowboardSet() {
        return snowboardSet;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Snowboards){
            return snowboardSet.equals(((Snowboards) o).getSnowboardSet());
        }
        return false;
    }
}
