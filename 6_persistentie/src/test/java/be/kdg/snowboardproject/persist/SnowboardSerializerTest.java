package be.kdg.snowboardproject.persist;

import be.kdg.snowboardproject.data.Data;
import be.kdg.snowboardproject.model.Snowboard;
import be.kdg.snowboardproject.model.Snowboards;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class SnowboardSerializerTest {
    private Snowboards snowboards;
    private SnowboardSerializer snowboardSerializer;

    @BeforeEach
    void setUp() {
        snowboards = new Snowboards();
        Data.getData().forEach(snowboards::add);
        snowboardSerializer = new SnowboardSerializer("./db/snowboards.ser");
    }

    @Test
    void testSerialize() {
        assertDoesNotThrow(() -> snowboardSerializer.serialize(snowboards), "Serialize gooit exception");
    }

    @Test
    void testDeserialize() {
        try{
            Snowboards snowboards1 = snowboardSerializer.deserialize();
            assertEquals(snowboards, snowboards1, "Snowboards zijn niet hetzelfde!");
        }
        catch(IOException | ClassNotFoundException e){
            fail(e);
        }
    }
}