package be.kdg.snowboardproject.persist;

import be.kdg.snowboardproject.data.Data;
import be.kdg.snowboardproject.model.RidingStyle;
import be.kdg.snowboardproject.model.Snowboard;
import org.junit.jupiter.api.*;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class SnowboardDbDaoTest {
    private static SnowboardDbDao db;

    @BeforeAll
    static void beforeAll() {
        db = new SnowboardDbDao("./db/snowboards.ser");
    }

    @AfterAll
    static void afterAll(){
        db.close();
    }

    @BeforeEach
    void setUp() {
        Data.getData().forEach(db::insert);
    }

    @AfterEach
    void afterEach(){
        db.delete("*");
    }

    @Test
    void testInsert() {
        Snowboard snowboard = new Snowboard("testl", "test123", 100, 150, RidingStyle.JIB, LocalDate.of(2020, 11, 5));
        assertTrue(db.insert(snowboard), "Insert niet gelukt!");
        //assertDoesNotThrow(() -> db.insert(snowboard));
    }

    @Test
    void testRetrieveUpdate() {
        Snowboard snowboard = db.retrieve("CUSTOM FLYING V 166W");
        assertNotNull(snowboard);

        snowboard.setBrand("test9565542");

        assertTrue(db.update(snowboard));
        assertEquals(db.retrieve("CUSTOM FLYING V 166W").getBrand(), "test9565542");
    }

    @Test
    void testDelete() {
        Snowboard snowboard = db.retrieve("CUSTOM FLYING V 166W");
        assertNotNull(snowboard);

        assertTrue(db.delete("CUSTOM FLYING V 166W"));
        assertFalse(db.delete("CUSTOM FLYING V 166W"));
    }

    @Test
    void testSort() {
        List<Snowboard> snowboardFromDb = db.sortedOn("SELECT * FROM snowboards ORDER BY brand");
        List<Snowboard> snowboardList = Data.getData();
        snowboardList = snowboardList.stream().distinct().collect(Collectors.toList());
        snowboardList.sort(Comparator.comparing(Snowboard::getBrand));
        assertEquals(snowboardFromDb, snowboardList);
    }




}