package be.kdg.snowboardproject.parsing;
import static org.junit.jupiter.api.Assertions.*;

import be.kdg.snowboardproject.data.Data;
import be.kdg.snowboardproject.model.Snowboards;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ParserTest {
    private final Snowboards snowboards = new Snowboards();

    @BeforeEach
    void setUp() {
        Data.getData().stream().distinct().forEach(snowboards::add);
    }

    @Test
    void testStaxDom() {
        assertDoesNotThrow(() -> new SnowboardStaxParser(snowboards, "./datafiles/staxSnowboards.xml"));
        final Snowboards[] snowboardsList = new Snowboards[1];
        assertDoesNotThrow(() -> snowboardsList[0] = SnowboardDomParser.domReadXML("./datafiles/staxSnowboards.xml"));
        assertEquals(snowboards.getSnowboardList(), snowboardsList[0].getSnowboardList());
    }

    @Test
    void testJaxb() {
        assertDoesNotThrow(() -> SnowboardJaxbParser.JaxbWriteXml("./datafiles/JaxbSnowboards.xml", snowboards));
        final Snowboards[] snowboardsList = new Snowboards[1];
        assertDoesNotThrow(() -> snowboardsList[0] = SnowboardJaxbParser.JaxbReadXml("./datafiles/JaxbSnowboards.xml", Snowboards.class));
        assertEquals(snowboards.getSnowboardList(), snowboardsList[0].getSnowboardList());
    }

    @Test
    void testGson() {
        assertDoesNotThrow(() -> SnowboardGsonParser.writeJson(snowboards, "./datafiles/Snowboards.json"));
        assertDoesNotThrow(() -> SnowboardGsonParser.readJson("./datafiles/Snowboards.json"));
        assertEquals(snowboards.getSnowboardList(), SnowboardGsonParser.readJson("./datafiles/Snowboards.json").getSnowboardList());
    }
}
