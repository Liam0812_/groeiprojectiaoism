package be.kdg.snowboardproject.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

@XmlRootElement(name = "snowboards")
public class Snowboards {
    private List<Snowboard> snowboardList = new ArrayList<>();
    boolean added;
    boolean removed;

    public boolean add (Snowboard snowboard){
        added = snowboardList.add(snowboard);
        return added;
    }

    public boolean remove(String model){
        Iterator<Snowboard> it = snowboardList.iterator();
        while(it.hasNext()){
            Snowboard snowboard = it.next();
            if(snowboard.getModel().equals(model)){
                it.remove();
                removed = true;
            }
        }
        return removed;
    }

    public Snowboard search(String model){
        Snowboard searchedBoard = new Snowboard();
        for(Snowboard snowboard: snowboardList){
            if(snowboard.getModel().equals(model)){
                searchedBoard = snowboard;
            }
        }
        return searchedBoard;
    }

    public List<Snowboard> sortedOnBrand(){
        List<Snowboard> snowboards = new ArrayList<>();
        snowboards.addAll(snowboardList);

        Collections.sort(snowboards, new Comparator<Snowboard>() {
            @Override
            public int compare(Snowboard o1, Snowboard o2) {
                return o1.getBrand().compareTo(o2.getBrand());
            }
        });
        return snowboards;
    }

    public List<Snowboard> sortedOnProductionDate(){
        List<Snowboard> snowboards = new ArrayList<>();
        snowboards.addAll(snowboardList);

        Collections.sort(snowboards, new Comparator<Snowboard>() {
            @Override
            public int compare(Snowboard o1, Snowboard o2) {
                return o1.getProductionDate().compareTo(o2.getProductionDate());
            }
        });
        return snowboards;
    }

    public List<Snowboard> sortedOnLength(){
        List<Snowboard> snowboards = new ArrayList<>();
        snowboards.addAll(snowboardList);

        Collections.sort(snowboards, new Comparator<Snowboard>() {
            @Override
            public int compare(Snowboard o1, Snowboard o2) {
                return o1.getLength() - o2.getLength();
            }
        });
        return snowboards;
    }

    @XmlElement(name= "snowboard")
    public void setSnowboardList(List<Snowboard> snowboardList) {
        this.snowboardList = snowboardList;
    }

    public List<Snowboard> getSnowboardList() {
        return snowboardList;
    }

    public int getSize(){
        return snowboardList.size();
    }
}
