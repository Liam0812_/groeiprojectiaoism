package be.kdg.snowboardproject.parsing;

import be.kdg.snowboardproject.model.Snowboards;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.GsonBuildConfig;

import java.io.*;

public class SnowboardGsonParser {
    static GsonBuilder builder = new GsonBuilder();
    static Gson gson = builder.setPrettyPrinting().create();

    public static void writeJson(Snowboards snowboards, String fileName){
        try(FileWriter jsonWriter = new FileWriter(fileName)){
            jsonWriter.write(gson.toJson(snowboards));
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    public static Snowboards readJson(String fileName){
        try(BufferedReader data = new BufferedReader(new FileReader(fileName))){
            return gson.fromJson(data, Snowboards.class);
        }
        catch(IOException e){
            e.printStackTrace();
            return null;
        }
    }

}
