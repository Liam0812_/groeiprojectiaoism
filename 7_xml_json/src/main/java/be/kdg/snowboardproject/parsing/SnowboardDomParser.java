package be.kdg.snowboardproject.parsing;

import be.kdg.snowboardproject.model.RidingStyle;
import be.kdg.snowboardproject.model.Snowboard;
import be.kdg.snowboardproject.model.Snowboards;

//import javax.swing.text.Element;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SnowboardDomParser {

    public static Snowboards domReadXML(String fileName){
        Snowboards snowboards = new Snowboards();

        try{
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(new File(fileName));
            doc.getDocumentElement().normalize();
            Element rootElement = (Element) doc.getDocumentElement();
            NodeList snowboardNodes = (NodeList) rootElement;

            for(int i=0; i<snowboardNodes.getLength(); i++){
                if(snowboardNodes.item(i).getNodeType() != Node.ELEMENT_NODE){
                    continue;
                }

                Element e = (Element) snowboardNodes.item(i);

                snowboards.add(new Snowboard(
                        e.getElementsByTagName("brand").item(0).getTextContent(),
                        e.getAttribute("model"),
                        Double.parseDouble(e.getElementsByTagName("price").item(0).getTextContent()),
                        Integer.parseInt(e.getElementsByTagName("length").item(0).getTextContent()),
                        RidingStyle.valueOf(e.getElementsByTagName("ridingstyle").item(0).getTextContent()),
                        LocalDate.parse(e.getElementsByTagName("production-date").item(0).getTextContent())
                ));
            }
            return snowboards;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
