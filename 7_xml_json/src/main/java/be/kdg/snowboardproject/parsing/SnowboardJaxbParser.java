package be.kdg.snowboardproject.parsing;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.awt.color.ProfileDataException;
import java.io.File;

public class SnowboardJaxbParser {


    public static void JaxbWriteXml(String file, Object root){
        try{
            JAXBContext context = JAXBContext.newInstance(root.getClass());

            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            m.marshal(root, new File(file));
        }
        catch(JAXBException e){
            e.printStackTrace();
        }
    }

    public static <T> T JaxbReadXml(String file, Class<T> typeParameterClass){
        try{
            JAXBContext jc = JAXBContext.newInstance(typeParameterClass);
            Unmarshaller u = jc.createUnmarshaller();
            File f = new File(file);
            return (T) u.unmarshal(f);
        }
        catch(JAXBException e){
            e.printStackTrace();
            return null;
        }
    }
}
