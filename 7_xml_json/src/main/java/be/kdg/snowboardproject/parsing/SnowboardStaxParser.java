package be.kdg.snowboardproject.parsing;

import be.kdg.snowboardproject.model.RidingStyle;
import be.kdg.snowboardproject.model.Snowboard;
import be.kdg.snowboardproject.model.Snowboards;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class SnowboardStaxParser {
    private XMLStreamWriter xmlStreamWriter;
    private Snowboards snowboards;

    public SnowboardStaxParser(Snowboards snowboards, String path){
        this.snowboards = snowboards;

        try{
            FileWriter file = new FileWriter(path, StandardCharsets.UTF_8);
            xmlStreamWriter = XMLOutputFactory.newInstance().createXMLStreamWriter(file);
            xmlStreamWriter = new IndentingXMLStreamWriter(xmlStreamWriter); //pretty printing
            staxWriteXML();
        }catch(XMLStreamException | IOException e){
            e.printStackTrace();
        }
    }

    public void staxWriteXML(){
        try{
            xmlStreamWriter.writeStartDocument();
            xmlStreamWriter.writeStartElement("snowboards");
            snowboards.getSnowboardList().forEach(this::writeElement);
            xmlStreamWriter.writeEndElement();
            xmlStreamWriter.writeEndDocument();
            xmlStreamWriter.close();
        }
        catch(XMLStreamException e){
            e.printStackTrace();
        }
    }

    public void writeElement(Snowboard snowboard){
        try{
            xmlStreamWriter.writeStartElement("snowboard");
            xmlStreamWriter.writeAttribute("model", snowboard.getModel());

            xmlStreamWriter.writeStartElement("brand");
            xmlStreamWriter.writeCharacters(snowboard.getBrand());
            xmlStreamWriter.writeEndElement();

            xmlStreamWriter.writeStartElement("model");
            xmlStreamWriter.writeCharacters(snowboard.getModel());
            xmlStreamWriter.writeEndElement();

            xmlStreamWriter.writeStartElement("price");
            xmlStreamWriter.writeCharacters(Double.toString(snowboard.getPrice()));
            xmlStreamWriter.writeEndElement();

            xmlStreamWriter.writeStartElement("length");
            xmlStreamWriter.writeCharacters(Integer.toString(snowboard.getLength()));
            xmlStreamWriter.writeEndElement();

            xmlStreamWriter.writeStartElement("ridingstyle");
            xmlStreamWriter.writeCharacters(snowboard.getRidingstyle().name());
            xmlStreamWriter.writeEndElement();

            xmlStreamWriter.writeStartElement("production-date");
            xmlStreamWriter.writeCharacters(snowboard.getProductionDate().toString());
            xmlStreamWriter.writeEndElement();

            xmlStreamWriter.writeEndElement();
        }
        catch(XMLStreamException e){
            e.printStackTrace();
        }
    }
}
