package be.kdg.snowboardproject.threading;

import be.kdg.snowboardproject.model.Snowboard;
import be.kdg.snowboardproject.model.SnowboardFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SnowboardRunnable implements Runnable{
    List<Snowboard> snowboards;
    Predicate<Snowboard> predicate;

    public SnowboardRunnable(Predicate<Snowboard> predicate) {
        this.predicate = predicate;
    }

    @Override
    public void run() {
        snowboards = Stream.generate(SnowboardFactory::newRandomSnowboard).filter(predicate).limit(1000).collect(Collectors.toList());
    }

    public List<Snowboard> getSnowboards() {
        return snowboards;
    }
}
