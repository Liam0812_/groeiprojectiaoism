package be.kdg.snowboardproject.threading;

import be.kdg.snowboardproject.model.Snowboard;

import java.util.List;
import java.util.function.Predicate;

public class SnowboardAttacker implements Runnable{
    List<Snowboard> geviseerdeList;
    Predicate<Snowboard> predicate;

    public SnowboardAttacker(List<Snowboard> geviseerdeList, Predicate<Snowboard> predicate) {
        this.geviseerdeList = geviseerdeList;
        this.predicate = predicate;
    }

    @Override
    public void run() {
        synchronized(geviseerdeList){
            geviseerdeList.removeIf(predicate);
        }
    }
}
