package be.kdg.snowboardproject.model;

import java.time.LocalDate;
import java.util.Objects;

public final class Snowboard implements Comparable<Snowboard> {
    private final String brand;
    private final String model;
    private final double price;
    private final int length;
    private final RidingStyle ridingstyle;
    private final LocalDate productionDate;

    public Snowboard(String brand, String model, double price, int length, RidingStyle ridingstyle, LocalDate productionDate) {
        this.brand = brand;
        this.model = model;
        this.price = price;
        this.length = length;
        this.ridingstyle = ridingstyle;
        this.productionDate = productionDate;
    }

    public Snowboard() {
        this("Ongekend", "Ongekend", 1, 1, RidingStyle.UNKNOWN, LocalDate.of(2010, 1, 1));
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public double getPrice() {
        return price;
    }

    public int getLength() {
        return length;
    }

    public RidingStyle getRidingstyle() {
        return ridingstyle;
    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Snowboard snowboard = (Snowboard) o;
        return brand.equals(snowboard.brand) &&
                model.equals(snowboard.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, model);
    }

    @Override
    public int compareTo(Snowboard o) {
        return this.brand.compareTo(o.brand) - this.model.compareTo(o.model);
    }

    @Override
    public String toString() {
        return String.format("Brand: %-20s Model: %-25s RidingStyle: %-18s Length (in cm): %-8d Productiondate: %-15s Prijs (euro): %.2f", brand, model, ridingstyle, length, productionDate, price);
    }
}
