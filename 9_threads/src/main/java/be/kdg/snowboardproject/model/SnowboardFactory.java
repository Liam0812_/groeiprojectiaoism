package be.kdg.snowboardproject.model;

import java.time.LocalDate;
import java.util.Random;

public class SnowboardFactory {

    private SnowboardFactory() {

    }

    public static Snowboard newEmptySnowboard() {
        return new Snowboard();
    }

    public static Snowboard newFilledSnowboard(String brand, String model, double price, int length, RidingStyle ridingstyle, LocalDate productionDate) {
        return new Snowboard(brand, model, price, length, ridingstyle, productionDate);
    }

    public static Snowboard newRandomSnowboard() {
        String randomBrand = generateString(7, 1, false);
        String randomModel = generateString(7, 1, false);

        return new Snowboard(randomBrand, randomModel, generatePrice(), generateLength(), generateRidingStyle(), generateProductionDate());
    }

    private static String generateString(int maxWordLength, int wordCount, boolean camelCase) {
        Random randomChar = new Random();
        Random randomChance = new Random();
        StringBuilder string = new StringBuilder();
        String klinker = "aeiou";
        String medeklinker = "bcdfghjklmnpqrstvwxyz";

        char character;

        for (int i = 0; i <= wordCount; i++) {
            for (int j = 0; j <= maxWordLength; j++) {
                int chance = randomChance.nextInt(3) + 1;
                if (chance == 1) {
                    character = klinker.charAt(randomChar.nextInt(klinker.length()));
                } else {
                    character = medeklinker.charAt(randomChar.nextInt(medeklinker.length()));
                }
                if (i != 0 && j == 0 && camelCase) {
                    Character.toUpperCase(character);
                }
                string.append(character);
            }
        }
        return string.toString();
    }

    private static double generatePrice(){
        double start = 40;
        double end = 300;

        double random = new Random().nextDouble();
        double price = start + (random * (end - start));
        price = (double) Math.round(price * 100)/100;

        return price;
    }

    private static int generateLength(){
        int start = 100;
        int end = 300;

        Random random = new Random();
        return start + random.nextInt(end - start + 1);
    }

    private static LocalDate generateProductionDate() {
        Random random = new Random();
        int day = random.nextInt(27) + 1;
        int month = random.nextInt(12) + 1;
        int year = random.nextInt(28) + 1992;
        return LocalDate.of(year, month, day);
    }

    private static RidingStyle generateRidingStyle(){
        Random random = new Random();
        RidingStyle style = null;
        switch(random.nextInt(4)){
            case 0:
                style = RidingStyle.ALL_MOOUNTAIN;
                break;
            case 1:
                style = RidingStyle.FREERIDE;
                break;
            case 2:
                style = RidingStyle.FREESTYLE;
                break;
            case 3:
                style = RidingStyle.JIB;
                break;
        }
        return style;
    }
}
