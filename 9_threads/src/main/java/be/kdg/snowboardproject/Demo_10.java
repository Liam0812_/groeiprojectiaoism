package be.kdg.snowboardproject;

import be.kdg.snowboardproject.model.RidingStyle;
import be.kdg.snowboardproject.model.Snowboard;
import be.kdg.snowboardproject.model.SnowboardFactory;
import be.kdg.snowboardproject.threading.SnowboardAttacker;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Demo_10 {
    public static void main(String[] args) {
        List<Snowboard> snowboards = Stream.generate(SnowboardFactory::newRandomSnowboard).limit(1000).collect(Collectors.toList());

        SnowboardAttacker run1 = new SnowboardAttacker(snowboards, snowboard -> snowboard.getLength() > 120);
        SnowboardAttacker run2 = new SnowboardAttacker(snowboards, snowboard -> snowboard.getPrice() > 60);
        SnowboardAttacker run3 = new SnowboardAttacker(snowboards, snowboard -> snowboard.getRidingstyle() == RidingStyle.FREESTYLE);

        Thread th1 = new Thread(run1, "th1");
        Thread th2 = new Thread(run2, "th2");
        Thread th3 = new Thread(run3, "th3");

        th1.start();
        th2.start();
        th3.start();

        System.out.println("Na uitzuivering");
        System.out.println("Aantal lengte < 120: " + snowboards.stream().filter(snowboard -> snowboard.getLength() > 120).count());
        System.out.println("Aantal prijs < 60: " + snowboards.stream().filter(snowboard -> snowboard.getPrice() > 60).count());
        System.out.println("Aantal FREESTYLE: " + snowboards.stream().filter(snowboard -> snowboard.getRidingstyle() == RidingStyle.FREESTYLE).count());


    }
}
