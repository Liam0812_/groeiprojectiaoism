package be.kdg.snowboardproject;

import be.kdg.snowboardproject.model.RidingStyle;
import be.kdg.snowboardproject.threading.SnowboardRunnable;

import java.util.ArrayList;
import java.util.List;

public class Demo_9 {
    private static int TESTCOUNT = 0;

    public static void main(String[] args) {
        long startTime;
        List<Long> timePerThread = new ArrayList<>();

        while (TESTCOUNT <= 100) {
            startTime = System.currentTimeMillis();
            SnowboardRunnable run1 = new SnowboardRunnable(snowboard -> snowboard.getLength() < 150);
            SnowboardRunnable run2 = new SnowboardRunnable(snowboard -> snowboard.getPrice() < 100);
            SnowboardRunnable run3 = new SnowboardRunnable(snowboard -> snowboard.getRidingstyle() == RidingStyle.ALL_MOOUNTAIN);

            Thread th1 = new Thread(run1, "thread1");
            Thread th2 = new Thread(run2, "thread2");
            Thread th3 = new Thread(run3, "thread3");

            th1.start();
            th2.start();
            th3.start();

            try {
                th1.join();
                th2.join();
                th3.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            run1.getSnowboards().stream().limit(5).forEach(System.out::println);
            run2.getSnowboards().stream().limit(5).forEach(System.out::println);
            run3.getSnowboards().stream().limit(5).forEach(System.out::println);

            timePerThread.add(System.currentTimeMillis() - startTime);
            TESTCOUNT++;
        }
        System.out.println("3 Threads verzamelen elk 1000 snowboards (gemiddelde uit 100 runs) " + timePerThread.stream().mapToDouble(Long::doubleValue).average().getAsDouble() + "ms");
    }

}
